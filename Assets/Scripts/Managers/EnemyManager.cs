﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : Singleton<EnemyManager>
{

   [SerializeField] private List<GameObject> _enemies;
   [SerializeField] private int _waveSize;
   [SerializeField] private float _timeBetweenWaves;
   [SerializeField] private float _timeBetweenSpawns;
   [SerializeField] private float _startDelay;

   protected override void Awake()
   {
      base.Awake();
      StartCoroutine(SpawnWaves());
   }

   protected override void OnDestroy()
   {
      base.OnDestroy();
   }

   private IEnumerator SpawnWaves()
   {
      yield return new WaitForSeconds(_startDelay);
      while (true)
      {
         for (int i = 0; i < _waveSize; i++)
         {
            GameObject enemy = _enemies[Random.Range(0, _enemies.Count)];
            Vector3 position = new Vector3(0,1,8);
            Quaternion rotation = Quaternion.identity;
            Instantiate(enemy,position,rotation);
            yield return new WaitForSeconds(_timeBetweenSpawns);
         }
         yield return new WaitForSeconds(_timeBetweenWaves);

         //Do until state of the game is GAMEOVER
      }
   }
}
