﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteBackground : MonoBehaviour
{

   [SerializeField] private float _speed;

   private void Update()
   {
      transform.position = new Vector3(0f,0f, Mathf.Repeat(Time.time * _speed, transform.lossyScale.y));
   }

}
