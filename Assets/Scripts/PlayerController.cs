﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
   [SerializeField] private Boundary _boundary;
   [SerializeField] private float _speed;
   [SerializeField] private float _tilt;
   private Rigidbody _rigidbody;

   private void Start()
   {
      _rigidbody = GetComponent<Rigidbody>();
   }

   private void FixedUpdate()
   {
      float horizontalMovement = Input.GetAxis("Horizontal");
      float verticalMovement = Input.GetAxis("Vertical");

      _rigidbody.velocity = new Vector3(horizontalMovement, 0f, verticalMovement) * _speed;

      transform.rotation = Quaternion.Euler(0.0f, 0.0f, _rigidbody.velocity.x * -_tilt);
      _rigidbody.position = new Vector3(
         Mathf.Clamp(_rigidbody.position.x, _boundary.xMin, _boundary.xMax),
         1f,
         Mathf.Clamp(_rigidbody.position.z, _boundary.zMin, _boundary.zMax)
         );
   }
}
