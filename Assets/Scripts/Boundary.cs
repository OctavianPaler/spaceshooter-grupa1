﻿using System;
using UnityEngine;

[Serializable]
public class Boundary
{
   [SerializeField] private float _xMin;
   [SerializeField] private float _xMax;
   [SerializeField] private float _zMin;
   [SerializeField] private float _zMax;

   public float xMin { get { return _xMin;} }
   public float xMax { get { return _xMax; } }
   public float zMin { get { return _zMin;} }
   public float zMax { get { return _zMax;} }
}
